const express = require('express');
const app = express();
const mainController = require('./controllers/mainController');

app.use('/', mainController);

const server = app.listen(8081, function() {
    console.log("Ứng dụng Node.js đang lắng nghe tại cổng 8081");
});
